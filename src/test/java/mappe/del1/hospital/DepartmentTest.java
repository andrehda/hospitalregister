package mappe.del1.hospital;
import mappe.del1.hospital.healthpersonal.Nurse;
import mappe.del1.hospital.exception.RemoveException;
import mappe.del1.hospital.healthpersonal.doctor.Surgeon;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

class DepartmentTest {
    /**
     * Adds a Surgeon then removes it
     * The test excpects there to be no elements in the arraylist since the only element was removed
     * @throws RemoveException
     */
    @Test
    void removeEmployee() throws RemoveException {
        Department department = new Department("Emergency");
        Surgeon surgeon = new Surgeon("Preston", "Mason", "123456");
        department.addEmployee(surgeon);
        department.remove(surgeon);
        ArrayList<Employee> employees = department.getEmployees();
        assertEquals(0,employees.size());
    }

    /**
     * Adds a Patient then removes it
     * The test excpects there to be no elements in the arraylist since the only element was removed
     * @throws RemoveException
     */
    @Test
    void removePatient() throws RemoveException {
        Department department = new Department("Emergency");
        Patient patient = new Patient("Preston", "Mason", "123456");
        department.addPatient(patient);
        department.remove(patient);
        ArrayList<Patient> patients = department.getPatients();
        assertEquals(0, patients.size());
    }

    /**
     * This test tries to remove a Nurse that isn't registered
     * The excpected result is that a RemoveException will be thrown
     */
        @Test
    void removePersonNotInRegsiter() {
        Nurse nurse = new Nurse("Preston", "Mason", "123456");
        Department department = new Department("Emergency");
        assertThrows(RemoveException.class, () -> department.remove(nurse));
    }
        @Test
    void addPatientSuccessfully() {
        Patient patient = new Patient("Preston", "Mason", "123456");
        Department department = new Department("Emergency");
        department.addPatient(patient);
        ArrayList<Patient> patients = department.getPatients();
        assertEquals(1,patients.size());
    }

    /**
     * This test tries to add a patient that already exists
     * It is expected to throw an IllegalArgumentException
     */
        @Test
    void addExistingPatient() {
        Patient patient1 = new Patient("Preston", "Mason", "123456");
        Department department = new Department("Emergency");
        department.addPatient(patient1);
        assertThrows(IllegalArgumentException.class, () -> department.addPatient(patient1));
    }
}