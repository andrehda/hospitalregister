package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import java.util.ArrayList;
import java.util.Objects;


 //The Patients and Employees are stored in an arraylist
public class Department {
    private String departmentName;
    private ArrayList<Patient> patients;
    private ArrayList<Employee> employees;
    public Department(String departmentName){
        this.departmentName =departmentName;
        patients = new ArrayList<>();
        employees = new ArrayList<>();
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<Employee> getEmployees(){
        return employees;
    }

    /**
     * This method first checks if the employee already exsists and throws an Illigal ar
     * @param employee
     */
    public void addEmployee(Employee employee) {
        if(employees.contains(employee)) {
            throw new IllegalArgumentException("This person is already an employee");
        }else{
            employees.add(employee);
        }
    }
    public ArrayList<Patient> getPatients(){
        return patients;
    }
    public void addPatient(Patient patient){
        if(patients.contains(patient)){
            throw new IllegalArgumentException("This person is already a patient");
     }else{
            patients.add(patient);
        }
    }

    /**
     * This method checks whether the person is an employee or a patient.
     * If the person is found the object is removed from its respective list
     * @param person
     * @throws RemoveException if the person isn't in the register
     */
    public void remove(Person person) throws RemoveException {
       if(employees.contains(person)&& person instanceof Employee){
           employees.remove(person);
       }else if(patients.contains(person) && person instanceof Patient){
           patients.remove(person);
       }else{
           throw new RemoveException("The person was not found");
       }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return departmentName.equals(that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName);
    }

    @Override
    public String toString() {
        return "Department{" +
                "deparmentName='" + departmentName + '\'' +
                '}';
    }
}
