package mappe.del1.hospital;

import java.util.ArrayList;

public class Hospital {
    private String hospitalName;
    private ArrayList<Department> departments;
    public Hospital(String hospitalName){
        this.hospitalName=hospitalName;
        departments= new ArrayList<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }
    public ArrayList<Department> getDepartments(){
        return departments;
    }

    @Override
    public String toString() {
        return "Hospital{" +
                "hospitalName='" + hospitalName + '\'' +
                '}';
    }
}
