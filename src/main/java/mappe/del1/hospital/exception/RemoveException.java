package mappe.del1.hospital.exception;

public class RemoveException extends Exception {
    public RemoveException(String message){
        super(message);
    }
}
