package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

public class HospitalClient {
    public static void main(String[] args){
        Hospital hospital = new Hospital("St.Olavs");
        HospitalTestData.fillRegisterWithTestData(hospital);
        Department emergency = hospital.getDepartments().get(0);
        Department childrenPolyClinic = hospital.getDepartments().get(1);
         //Removes an employee from the system
        try{
            Employee Anton = emergency.getEmployees().get(2);
            emergency.remove(Anton);
        } catch (RemoveException e) {
            e.printStackTrace();
        }


         //Tries to remove a Patient that isn't registered, then it throws a RemoveException
        try{
            Patient Hans =  new Patient("Hans", "Olav", "3");
            childrenPolyClinic.remove(Hans);
        } catch (RemoveException e) {
            e.printStackTrace();
        }
    }
}
