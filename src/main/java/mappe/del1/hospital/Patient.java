package mappe.del1.hospital;

public class Patient extends Person implements Diagnosable{
    public String diagnosis;

    protected Patient( String firstName, String lastName, String socialSecurityNumber) {
        super( firstName, lastName, socialSecurityNumber);
    }
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "diagnosis='" + diagnosis + '\'' +
                '}';
    }
}
